//
//  LaunchViewController.swift
//  Leliel
//


import UIKit
import Hero

var allData:NSMutableDictionary!

class LaunchViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        allData = [
            "Cosplays":
                [
                    ["Sombra's Gun":
                        ["Sombra_1", "Sombra_2", "Sombra_3", "Sombra_4", "Sombra_5", "Sombra_6", "Sombra_7", "Sombra_8"]
                    ],
                    ["Mercy's Staff":
                        ["Mercy_1", "Mercy_2", "Mercy_3", "Mercy_4", "Mercy_5", "Mercy_6", "Mercy_7"]
                    ]
            ],
            "Costumes":
                [
                    ["Antlers":
                        ["Antlers_1", "Antlers_2", "Antlers_3", "Antlers_4"]
                    ],
                    ["Angels Wings":
                        ["AngelsWings_1", "AngelsWings_2", "AngelsWings_3", "AngelsWings_4"]
                    ]
            ]
        ]
        
        self.animateLogo()
        
    }
    
    // MARK: - Animation
    func animateLogo()
    {
        UIView.animate(withDuration: 1,
                       delay: 0.2,
                       options: UIView.AnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        
                        self.logoImageView.alpha = 1
                        
        }, completion: { (finished) -> Void in
            
            self.nameBottomConstraint.constant = 20
            UIView.animate(withDuration: 0.5,
                           delay: 1,
                           options: UIView.AnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            
                            self.view.layoutIfNeeded()
                            
            }, completion: { (finished) -> Void in
                
                let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                homeVC.hero.modalAnimationType = .selectBy(presenting: .zoom, dismissing: .zoom)
                self.present(homeVC, animated: true, completion: nil)
                
            })
            
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
