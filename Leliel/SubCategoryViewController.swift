//
//  SubCategoryViewController.swift
//  Leliel
//


import UIKit
import SideMenu

class SubCategoryViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var names = [String]()
    var images = [UIImage]()
    var selectedIndex:Int!
    var selectedCat:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.selectedCat == "Cosplays"
        {
            self.names = [
                "Sombra's Gun",
                "Mercy's Staff"]
            self.images = [
                #imageLiteral(resourceName: "MercyMain"),
                #imageLiteral(resourceName: "Mercy_7")]
        }
        else
        {
            self.names = [
                "Antlers",
                "Angels Wings"]
            self.images = [
                #imageLiteral(resourceName: "AntlersMain"),
                #imageLiteral(resourceName: "AngelsWings_4")]
        }
        
        self.setupSideMenu()
    }
    
    @objc func goToNextPage(selectedPath:IndexPath)
    {
        let detailsVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        detailsVC.hero.modalAnimationType = .selectBy(presenting: .zoomSlide(direction: .left), dismissing: .zoomSlide(direction: .right))
        detailsVC.selectedCat = self.names[selectedPath.row]
        self.present(detailsVC, animated: true, completion: nil)
    }
    
    fileprivate func setupSideMenu()
    {
        SideMenuManager.default.menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? UISideMenuNavigationController
        
        SideMenuManager.default.menuPresentMode = .viewSlideInOut
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuBlurEffectStyle = .light
    }
    
    // MARK: - Actions
    @IBAction func backClicked(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SubCategoryViewController:UICollectionViewDelegate, UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.names.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! SubCategoryCollectionViewCell
        
        cell.nameLabel.text = self.names[indexPath.row]
        cell.subImageView.image = self.images[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        self.goToNextPage(selectedPath: indexPath)
    }
}
