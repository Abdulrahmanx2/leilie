//
//  HomeViewController.swift
//  Leliel
//


import UIKit
import Hero
import SideMenu

class HomeViewController: UIViewController {

    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var categoriesNames = [String]()
    var categoriesImages = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.backImageView.layer.cornerRadius = 5
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 0.01))
        
        self.setupSideMenu()
        
        self.categoriesNames = allData.allKeys as! [String]
        
        self.categoriesImages = [
            #imageLiteral(resourceName: "SombraMain"),
            #imageLiteral(resourceName: "AngelWingsMain")]
    }

    @objc func goToNextPage(selectedPath:IndexPath)
    {
        let subVC = self.storyboard?.instantiateViewController(withIdentifier: "SubCategoryViewController") as! SubCategoryViewController
        subVC.hero.modalAnimationType = .selectBy(presenting: .zoomSlide(direction: .left), dismissing: .zoomSlide(direction: .right))
        subVC.selectedIndex = selectedPath.row
        subVC.selectedCat = self.categoriesNames[selectedPath.row]
        self.present(subVC, animated: true, completion: nil)
    }
    
    fileprivate func setupSideMenu()
    {
        SideMenuManager.default.menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? UISideMenuNavigationController
        
        SideMenuManager.default.menuPresentMode = .menuDissolveIn
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuBlurEffectStyle = .light
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeViewController:UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.categoriesNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! HomeTableViewCell
        
        cell.nameLabel.text = self.categoriesNames[indexPath.row]
        cell.catImageView.image = self.categoriesImages[indexPath.row]
        cell.catImageView.layer.cornerRadius = 5
        cell.catImageView.clipsToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.goToNextPage(selectedPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
