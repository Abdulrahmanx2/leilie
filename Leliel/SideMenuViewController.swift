//
//  SideMenuViewController.swift
//  Leliel
//


import UIKit
import SideMenu

class SideMenuViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var names = [String]()
    var images = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.names = [
            "Home",
            "Contact",
            "About"]
        
        self.images = [
            #imageLiteral(resourceName: "Home"),
            #imageLiteral(resourceName: "Contact"),
            #imageLiteral(resourceName: "About")]
    }
    
    // MARK: - Actions
    @IBAction func backClicked(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SideMenuViewController:UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SideMenuTableViewCell
        
        cell.nameLabel.text = self.names[indexPath.row]
        cell.sideImageView.image = self.images[indexPath.row]
        
        return cell
    }
}
