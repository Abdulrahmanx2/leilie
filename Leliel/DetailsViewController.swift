//
//  DetailsViewController.swift
//  Leliel
//


import UIKit
import SideMenu

class DetailsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var selectedIndex:Int!
    var selectedCat:String!
    
    var images = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.selectedCat == "Sombra's Gun"
        {
            self.images = ["Sombra_1", "Sombra_2", "Sombra_3", "Sombra_4", "Sombra_5", "Sombra_6", "Sombra_7", "Sombra_8"]
        }
        else if self.selectedCat == "Mercy's Staff"
        {
            self.images = ["Mercy_1", "Mercy_2", "Mercy_3", "Mercy_4", "Mercy_5", "Mercy_6", "Mercy_7"]
        }
        else if self.selectedCat == "Antlers"
        {
            self.images = ["Antlers_1", "Antlers_2", "Antlers_3", "Antlers_4"]
        }
        else if self.selectedCat == "Angels Wings"
        {
            self.images = ["AngelsWings_1", "AngelsWings_2", "AngelsWings_3", "AngelsWings_4"]
        }
        
        self.setupSideMenu()
    }
    
    fileprivate func setupSideMenu()
    {
        SideMenuManager.default.menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? UISideMenuNavigationController
        
        SideMenuManager.default.menuPresentMode = .viewSlideInOut
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuBlurEffectStyle = .light
    }
    
    // MARK: - Actions
    @IBAction func backClicked(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DetailsViewController:UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.images.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DetailsTableViewCell
        
        cell.detailsImageView.image = UIImage(named: self.images[indexPath.row])
        
        return cell
    }
}
