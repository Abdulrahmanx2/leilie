//
//  SubCategoryCollectionViewCell.swift
//  Leliel
//


import UIKit

class SubCategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var subImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
}
