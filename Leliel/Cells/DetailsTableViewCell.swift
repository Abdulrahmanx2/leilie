//
//  DetailsTableViewCell.swift
//  Leliel
//


import UIKit

class DetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var detailsImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
